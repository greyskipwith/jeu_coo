from .action import Action2, Action3
from mud.events import CutableEvent

class CutableAction(Action2):
    EVENT = CutableEvent
    ACTION = "cut"
    RESOLVE_OBJECT = "resolve_for_use"

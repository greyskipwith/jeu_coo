from .event import Event2

class CutableEvent(Event2):
    NAME = "cut"

    def perform(self):
        if not self.object.has_prop("cutable"):
            self.fail()
            return self.inform("cutable.failed")
        self.inform("cutable")
